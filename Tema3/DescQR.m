function [x,q,r] = DescQR(A,b)
    n = length(b);
    q = zeros(n);
    r = zeros(n);
    sum = 0;
    for i = 1:n
        sum = sum + A(i,1)^2;
    end
    r(1,1) = sqrt(sum);
    for i = 1:n
        q(i,1) = A(i,1)/r(1,1);
    end
    for j = 2:n
        sum = 0;
        for s = 1:n
            sum = sum + q(s,1)*A(s,j);
        end
        r(1,j) = sum;
    end
    for k = 2:n
        sum1 = 0;
        for i=1:n
            sum1 = sum1 + A(i,k)^2;
        end
        sum2 = 0;
        for s=1:(k-1)
            sum2 = sum2 + r(s,k)^2;
        end
        r(k,k) = sqrt(sum1-sum2);
        for i = 1:n
            sum = 0;
            for s = 1:(k-1)
                sum = sum + q(i,s)*r(s,k);
            end
            q(i,k) = 1/r(k,k)*(A(i,k) - sum);
        end
        for j = (k+1):n
            sum = 0;
            for s = 1:n
                sum = sum + q(s,k)*A(s,j);
            end
            r(k,j) = sum;
        end
    end
    x = SubsDesc(r,q.'*b);
end

