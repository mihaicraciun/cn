function [x,l,u] = DescLU(A,b)
   n = length(b);
   for j = 1:n
       u(1,j)=A(1,j);
   end
   if u(1,1) == 0
       disp('A nu admite fact. LU');
       return;
   end
   for i = 1:n
       l(i,1) = A(i,1)/u(1,1);
   end
   for k = 2:n
       for j = k:n
            sum = 0;
            for s = 1:(k-1)
                sum = sum + l(k,s)*u(s,j);
            end
            u(k,j) = A(k,j) - sum;
       end
       if u(k,k) == 0
           disp('A nu admite fact. LU');
           return;
       end
       for i = k:n
           sum = 0;
           for s = 1:(k-1)
               sum = sum + l(i,s)*u(s,k);
           end
           l(i,k)=1/u(k,k)*(A(i,k)-sum);
       end
   end
   y = SubsAsc(l,b);
   x = SubsDesc(u,y);
end

