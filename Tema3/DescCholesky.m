function [l,x] = DescCholesky(A,b)
    n = length(b);
    a = A(1,1);
    if a <= 0
        disp('A nu este pozitiv definita');
        return;
    end
    l(1,1) = sqrt(A(1,1));
    for i = 2:n
        l(i,1) = A(i,1)/l(1,1);
    end
    for k = 2:n
        sum = 0;
        for s = 1:(k-1)
            sum = sum + l(k,s)*l(k,s);
        end
        a = A(k,k) - sum;
        if a <=0
            disp('A nu este pozitiv definita');
            return;
        end
        l(k,k) = sqrt(a);
        for i = (k+1):n
            sum = 0;
            for s = 1:(k-1)
                sum = sum + l(i,s)*l(k,s);
            end
            l(i,k) = 1/l(k,k)*(A(i,k)-sum);
        end
    end
    y=SubsAsc(l,b);
    x=SubsDesc(l.',y);
end

