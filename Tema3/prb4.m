A1 = [1 2 4;3 8 14;2 6 13];
A2 = [4 2 2;2 10 4;2 4 6];
A3 = [0 4 5;-1 -2 -3;0 0 1];

fprintf('Matricea 1\n\n');
b = [rand*10; rand*10; rand*10]

[x,l,u] = DescLU(A1,b)
%[l,x] = DescCholesky(A1,b) % Nu este pozitiv definita
[x,q,r] = DescQR(A1,b)

fprintf('Matricea 2\n\n');
b = [rand*10; rand*10; rand*10]

[x,l,u] = DescLU(A2,b)
[l,x] = DescCholesky(A2,b)
[x,q,r] = DescQR(A2,b)

fprintf('Matricea 3\n\n');
b = [rand*10; rand*10; rand*10]

%[x,l,u] = DescLU(A3,b) % Nu admite descompunere LU
%c[l,x] = DescCholesky(A3,b) % Nu este pozitiv definita
[x,q,r] = DescQR(A3,b)

