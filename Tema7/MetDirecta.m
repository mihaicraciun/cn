function y = MetDirecta(X,Y,x)
n = length(X)- 1; %Gradul polinomului Pn
A = zeros(n+1);
for i = 1:n+1
    for j = 1:n+1
        A(i, j) = X(i)^(j-1);
    end
end
a = GaussPivTot(A,Y);
Pn = 0;
for i = 1:n+1
    Pn = Pn + a(i)*x^(i-1);
end
y = Pn;
end