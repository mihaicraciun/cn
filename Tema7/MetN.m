function y = MetN(X,Y,x)
n = length(X)- 1; %Gradul polinomului Pn
A = zeros(n+1);
for i = 1:n+1
    A(i,1) = 1;
end
for i = 2:n+1
    for j = 2:i
        p = 1;
        for k = 1:j-1
            p = p*(X(i)-X(k));
        end
        A(i ,j) = p;
    end
end
c = SubsAsc(A,Y);
Pn = c(1);
for i = 2:n+1
    p = 1;
    for j = 1:i-1
        p = p*(x-X(j));
    end
    Pn = Pn + c(i)*p;
y = Pn;
end