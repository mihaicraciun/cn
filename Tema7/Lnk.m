function lnk = Lnk(X,k,x)
n = length(X)- 1;
lnk = 1;
for i=1:n+1
    if i == k
        continue
    end
    lnk = lnk * (x-X(i))/(X(k)-X(i));
end
end

