function [ norm ] = Norm2(A)
lambda = eigs(A'*A);
norm = max(sqrt(lambda));
end

