A = [1 -3 3;3 -5 3;6 -6 4];

% a)
mynorm_a = Norm2(A)

% b)
my_cond_a = Norm2(A)*Norm2(A^-1)

% c)
norm_a = norm(A,2)
cond_a = cond(A,2)