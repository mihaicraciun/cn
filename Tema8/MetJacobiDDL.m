function [xaprox, N] = MetJacobiDDL(A, a, eps)
    n = size(A,1);
    for i = 1:n
       if abs(A(i,i)) <= sum(abs(A(i,1:n))) - abs(A(i,i))
           disp('Matr. nu este diag dom pe linii');
           break;
       end
    end
    x = [zeros(n,1)];
    k = 1;
    for i = 1:n
        for j = 1:n
            if i==j
                delta = 1;
            else
                delta = 0;
            end
            B(i,j) = delta - A(i,j)/A(i,i);
        end
        b(i,1) = a(i)/A(i,i);
    end
    q = norm(B,inf);
    cond = 1;
    while cond == 1
        k = k + 1;
        x = [x, B*x(:,k-1)+b];
        if q^k/(1-q)*norm(x(:,2) - x(:,1),inf) < eps
            cond = 0;
        end
    end
    xaprox = x(:,k);
    N = k;


