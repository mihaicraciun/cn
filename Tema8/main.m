clear all
f=inline('sin(x)','x');
fp = inline('cos(x)','x');
x0=-pi/2; xf=pi/2; 
N=2;
X=linspace(x0,xf,(N+1)); 
Y=f(X);
x=linspace(x0,xf,100);
fpa = fp(X(1));

fpb = fp(X(N+1));

for i = 1:length(x)
   %S(i) = SplineL(X,Y,x(i));
   S(i) = SplineP(X,Y,fpa,x(i));
     %S(i) = SplineC(X,Y,fpa,fpb,x(i));    
end

figure(1)
hold on
plot(x,S,'k','Linewidth',3);
xlabel('x')
ylabel('y')
grid on
plot(x,f(x),'--r','Linewidth',3);
plot(X,f(X),'o','MarkerFaceColor','g','MarkerSize',10)
legend('S(x) - spline','f(x)','noduri de interpolare')

Sp = diff(S)./diff(x);
figure(2)
plot(x(1:length(x)-1),Sp,'k','Linewidth',3)
df = diff(f(x))./diff(x);
hold on
plot(x(1:length(x)-1),df,'--r','Linewidth',3)
grid on
legend('derivata funciei S','derivata functiei f')
xlabel('x')
ylabel('y')

