function y = SplineC(X,Y,fpa,fpb,x)
    f = @(x) sin(x);
    f1 = @(x) -cos(x);
    f2 = @(x) -sin(x);
    n = length(X) - 1;
    B = zeros(n+1);
    B(1,1) = 1;
    B(n+1,n+1) = 1;
   for j = 1:n
       a(j) = Y(j);
    end
    for j = 2:n
        B(j,j-1) = 1;
        B(j,j) = 4;
        B(j,j+1) = 1;
    end
    bb(1,1) = fpa;
    h = X(2) - X(1);
    for j = 2:n
       bb(j,1) = 3/h*(Y(j+1)-Y(j-1)); 
    end
    bb(n+1,1) = fpb;
    b = MetJacobiDDL(B,bb,10^(-8));
    B*b
    b(n+1) = fpb;
    for i = 1:n
       h(j) = X(j+1)-X(j); 
    end
    for j = 1:n
       d(j) = -2*(Y(j+1)-Y(j))/(h(j)^3) + (b(j+1)+b(j))/(h(j)^2);
       c(j) = 3*(Y(j+1)-Y(j))/(h(j)^2) + (b(j+1)+2*b(j))/h(j);
    end
    for j = 1:n
       if x>=X(j) && x<=X(j+1)
          f(x)
          f1(x)
          f2(x)
          S = a(j) + b(j)*(x-X(j)) + c(j)*(x-X(j))^2 + d(j)*(x-X(j))^3;
          j = n+1;
       end
    end
    y = S;    
end

