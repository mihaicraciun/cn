function y = SplineP(X,Y,fpa,x)
    b(1) = fpa;
    n = length(X) - 1;
    for j = 1:n
       h(j) = X(j+1)-X(j); 
    end
    for j = 1:n-1
       b(j+1) = 2/h(j)*(Y(j+1)-Y(j)) - b(j);  
    end
    for j = 1:n
       c(j) = 1/(h(j)^2)*(Y(j+1)-Y(j)- h(j)*b(j));  
    end
    for j = 1:n
       a(j) = Y(j); 
    end 
    for j = 1:n
       if x>=X(j) && x<=X(j+1)
          S = a(j) + b(j)*(x-X(j)) + c(j)*(x-X(j))^2;
          j = n+1;
       end
    end
    y = S;
    
% for i = 1:length(x)    
%     b(1) = fpa;
%     n = length(X) - 1;
%     for j = 1:n
%        h(j) = X(j+1)-X(j); 
%     end
%     for j = 1:n-1
%        b(j+1) = 2/h(j)*(Y(j+1)-Y(j)) - b(j);  
%     end
%     for j = 1:n
%        c(j) = 1/(h(j)^2)*(Y(j+1)-Y(j)- h(j)*b(j));  
%     end
%     for j = 1:n
%        a(j) = Y(j); 
%     end 
%     for j = 1:n
%        if x(i)>=X(j) && x(i)<=X(j+1)
%           S = a(j) + b(j)*(x(i)-X(j)) + c(j)*(x(i)-X(j))^2;
%           j = n+1;
%        end
%     end
%     y(i) = S;
% end
end