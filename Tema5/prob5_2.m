A1 = [0.2 0.01 0;0 1 0.04;0 0.02 1];
A2 = [4 1 2;0 3 1;2 4 8];
a = [1;2;3];

disp('A1 jacobi');
[xaprox1, N1] = MetJacobi(A1,a,1e-5)
disp('A1 jacobiDLL');
[xaprox2, N2] = MetJacobiDDL(A1,a,1e-5)

disp('A2 jacobi nu asigura conv');
% [xaprox3, N3] = MetJacobi(A2,a,1e-5)
disp('A2 jacobiDLL');
[xaprox4, N4] = MetJacobiDDL(A2,a,1e-5)