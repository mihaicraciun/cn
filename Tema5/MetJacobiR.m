function [xaprox, N] = MetJacobiR(A,a,eps,s)
    n = size(A,1);
    
    B = zeros(n);
    b = zeros(n,1);
    
    for i = 1:n
        b(i) = s * a(i);
        for j = 1:n
            d = 0;
            if i == j
                d = 1;
            end
            B(i,j) = d - s * A(i,j);
        end
    end
    
    xprev = zeros(n,1);
    k = 0;
    
    k = k + 1;
    xcrt = B*xprev+b;
    
    while (normA(A,xcrt-xprev)/normA(A,xprev)>=eps)
        xprev = xcrt;
        k = k + 1;
        xcrt = B*xcrt+b;
    end
    
    xaprox = xcrt;
    N = k;
    return;
end