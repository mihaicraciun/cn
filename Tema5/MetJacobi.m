function [xaprox, N] = MetJacobi(A,a,eps)
    n = size(A,1);
    q = norm(eye(n)-A);
    if q>=1
        disp('Metoda jacobi nu asigura conv');
        return;
    end
    x0 = zeros(n,1);
    k=0;
    B=eye(n)-A;
    b=a;
    
    k=k+1;
    x1=B*x0+b;
    
    xcrt = x1;
    while((q^k/(1-q)) * norm(x1-x0) >= eps)
        k=k+1;
        xcrt=B*xcrt+b;
    end
    xaprox=xcrt;
    N=k;
end