function [ xoaprox, No, so ] = MetJacobiRO(A,a,eps,p)
    for s = 1:p-1
        sigs =  2*s/(norm(A,inf)*p);
        [~,N] = MetJacobiR(A,a,eps,sigs);
        V(s) = N;
    end
    [~,s] = min(V);
    so = 2*s/(norm(A,inf)*p);
    [xoaprox,No]=MetJacobiR(A,a,eps,so);
    return;
end

