function [xoaprox, No, so] = MetGaussSeidelRO(A, a, eps, p)
    for s = 1:p - 1
        sigs = 2 * s / (norm(A, inf) * p);
        [~, N] = MetGaussSeidelR(A, a, eps, sigs);
        V(s) = N;
    end
    [~, s] = min(V);
    so = 2 * s / (norm(A, inf) * p);
    [xoaprox, No] = MetGaussSeidelR(A, a, eps, so);
    return;
end

