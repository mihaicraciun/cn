function [xaprox, N] = MetJacobiDDL(A,a,eps)
    n = size(A,1);
    
    for i = 1:n 
        sum = 0;
        for j = 1:n
            if i~=j
                sum = sum + abs(A(i,j));
            end
        end
        if A(i,i) <= sum
            disp('Matr. nu este diag. dom. pe linii');
            return;
        end
    end
    
    x0 = zeros(n,1);
    k=0;
    
    B = zeros(n);
    
    for i = 1:n
        for j = 1:n
            d = 0;
            if i==j
                d = 1;
            end
            B(i,j) = d - A(i,j)/A(i,i);
        end
    end
    
    b = zeros(n,1);
    
    for i = 1:n 
        b(i)=a(i)/A(i,i);
    end
    
    q = norm(B,inf);
    
    k=k+1;
    x1=B*x0+b;
    
    xcrt = x1;
    while((q^k/(1-q)) * norm(x1-x0, inf) >= eps)
        k=k+1;
        xcrt=B*xcrt+b;
    end
    xaprox=xcrt;
    N=k;
end

