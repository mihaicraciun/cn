function [xaprox, N] = MetGaussSeidelR(A,a,eps,s)
    n = size(A,1);
    
    D = zeros(n);
    L = zeros(n);
    R = zeros(n);
    
    for i = 1:n
        for j = 1:n
            if i == j
                D(i,j) = A(i,j);
            elseif i > j
                L(i,j) = A(i,j);
            else
                R(i,j) = R(i,j);
            end 
        end
    end
    
    B = (s*L+D)^(-1)*((1-s)*D-s*R);
    b = (s*L+D)^(-1)*s*a;
    
    xprev = zeros(n,1);
    k = 0;
    
    k = k + 1;
    xcrt = B*xprev+b;
    
    while (normA(A,xcrt-xprev)/normA(A,xprev)>=eps)
        xprev = xcrt;
        k = k + 1;
        xcrt = B*xcrt+b;
    end
    
    xaprox = xcrt;
    N = k;
    return;
end