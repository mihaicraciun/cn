A = [0.2 0.01 0;0 1 0.04;0 0.02 1];
a = [1;2;3];
p = [10 20 50];

grid on
hold on

for i=1:3
    clearvars V S
    c=1;
    for s = 1:p(i)-1
        S(c)=s;
        c=c+1;
        
        sigs =  2*s/(norm(A,inf)*p(i));
        [~,N] = MetJacobiR(A,a,eps,sigs);
    V(s) = N;
    end
    
    plot(S,V,'Linewidth',3) %Constructia graficului  functiei
end

for i=1:3
    clearvars V S
    c=1;
    for s = 1:p(i)-1
        S(c)=s;
        c=c+1;
        
        sigs =  2*s/(max(eigs(A))*p(i));
        [~,N] = MetJacobiR(A,a,eps,sigs);
    V(s) = N;
    end
    
    plot(S,V,'--') %Constructia graficului  functiei
end

[xoaprox, NO] = MetJacobiRO(A,a,1e-5,50)