f = inline('x.^3 - 18*x - 10','x');
A=-5; 
B=5;
eps=10^(-3);
x=linspace(A,B,100);
y=f(x);
plot(x,y,'Linewidth',3) %Constructia graficului  functiei

grid on
hold on
plot([A B], [0 0],'k','Linewidth',2) %Reprezentarea axei Ox
plot([0 0], [min(y) max(y)],'k','Linewidth',2); %Reprezentarea axei Oy

r1 = MetPozFalse(f,-5,-2.5,eps)
r2 = MetPozFalse(f,-2.5,0,eps)
r3 = MetPozFalse(f,0,5,eps)
plot(r1,f(r1),'*')
plot(r2,f(r2),'*')
plot(r3,f(r3),'*')

xlabel('x')
ylabel('y')
title('f(x)=x^3 - 7*x^2 + 14*x - 6')