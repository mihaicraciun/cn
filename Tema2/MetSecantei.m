function [ xaprox ] = MetSecantei( f, a, b, x0, x1, epsilon )
%METSECANTEI Summary of this function goes here
%   Detailed explanation goes here
while abs(x1 - x0)/abs(x0) > epsilon
    x2 = (x0*f(x1) - x1*f(x0))/(f(x1) - f(x0));
    if x2 < a || x2 > b
        disp('sulfus');
        xaprox = nan;
        return;
    end
    x0 = x1;
    x1 = x2;
end
xaprox = x1;
end