% Sist 1
eps = 10^(-20) * randi(9);
A1 = [eps 1;1 1];
b1 = [1;2];

% Sist 2
C = 10^(20) * randi(9);
A2 = [2 2*C;1 1];
b2 = [2*C;2];

sol_1_fara = GaussFaraPiv(A1,b1)
sol_1_part = GaussPivPart(A1,b1)
sol_1_tot  = GaussPivTot(A1,b1)

sol_2_fara = GaussFaraPiv(A2,b2)
sol_2_part = GaussPivPart(A2,b2)
sol_2_tot  = GaussPivTot(A2,b2)