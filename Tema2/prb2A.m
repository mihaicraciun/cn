f = inline('x.^3 - 18*x - 10','x');
A=-5; 
B=5;
x=linspace(A,B,100);
y=f(x);
plot(x,y,'Linewidth',3) %Constructia graficului  functiei

grid on
hold on
plot([A B], [0 0],'k','Linewidth',2) %Reprezentarea axei Ox
plot([0 0], [min(y) max(y)],'k','Linewidth',2); %Reprezentarea axei Oy
xlabel('x')
ylabel('y')
title('f(x)=x^3 - 7*x^2 + 14*x - 6')