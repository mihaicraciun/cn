f = inline('8*x.^3 + 4*x - 1','x');
df = inline('24*x.^2 + 4','x');
A=0; B=1; eps = 10^(-5);

fprintf("\n\nNewton Raphson \n\n");
%Newton Raphson
x0=0.5;
k = 1;
x(k)=x0;
fprintf('x(%d) = %f\n\n',k,x(k));
while k<3
    k = k + 1;
    x(k)=x(k-1)-f(x(k-1))/df(x(k-1));
    fprintf('f(x(%d)) = %f\n',k-1,f(x(k-1)));
    fprintf('df(x(%d)) = %f\n',k-1,df(x(k-1)));
    fprintf('x(%d) = %f\n',k,x(k));
    fprintf('\n');
    if (abs(x(k)-x(k-1))/abs(x(k-1))<eps)
        break;
    end
end

clear;

f = inline('8*x.^3 + 4*x - 1','x');
df = inline('24*x.^2 + 4','x');
A=0; B=1; eps = 10^(-5);

fprintf("\n\nMetoda Secantei \n\n");
%Metoda Secantei
x0=0.2;
x1=0.5;
x(1)=x0;
x(2)=x1;
k=2;
    k=k+1;
    fprintf('x(%d) = %f\n',k-2,x(k-2));
    fprintf('x(%d) = %f\n',k-1,x(k-1));
    fprintf('f(x(%d)) = %f\n',k-2,f(x(k-2)));
    fprintf('f(x(%d)) = %f\n',k-1,f(x(k-1)));
    fprintf('prod1 = %f\nprod2 = %f\n',x(k-2)*f(x(k-1)),x(k-1)*f(x(k-2)));
    fprintf('diferenta numarator = %f\n',x(k-2)*f(x(k-1))-x(k-1)*f(x(k-2)));
    fprintf('diferenta numitor = %f\n',f(x(k-1))-f(x(k-2)));
    x(k)=(x(k-2)*f(x(k-1))-x(k-1)*f(x(k-2)))/(f(x(k-1))-f(x(k-2)));
    fprintf('x(3) = %f\n',x(k));
    
clear;

f = inline('8*x.^3 + 4*x - 1','x');
df = inline('24*x.^2 + 4','x');
A=0; B=1; eps = 10^(-5);

fprintf("\n\nMetoda Pozitiei False \n\n");
%Metoda Pozitiei false

k=1;
a(1)=A;
b(1)=B;
x(1)=(a(1)*f(b(1))-b(1)*f(a(1)))/(f(b(1))-f(a(1)));
fprintf('a(1) = %f\n',a(1));
fprintf('b(1) = %f\n',b(1));
fprintf('x(1) = %f\n\n',x(1));
k=k+1;
if f(x(k-1)) == 0
    x(k)=x(k-1);
        xaprox=x(k);
        return;
elseif f(a(k-1))*f(x(k-1)) < 0
    a(k)=a(k-1);
    b(k)=x(k-1);
    x(k)=(a(k)*f(b(k))-b(k)*f(a(k)))/(f(b(k))-f(a(k)));
elseif f(a(k-1))*f(x(k-1)) > 0
    a(k)=x(k-1);
    b(k)=b(k-1);
    x(k)=(a(k)*f(b(k))-b(k)*f(a(k)))/(f(b(k))-f(a(k)));
end
fprintf('a(2) = %f\n',a(2));
fprintf('b(2) = %f\n',b(2));
fprintf('x(2) = %f\n\n',x(2));
k=k+1;
if f(x(k-1)) == 0
    x(k)=x(k-1);
        xaprox=x(k);
        return;
elseif f(a(k-1))*f(x(k-1)) < 0
    a(k)=a(k-1);
    b(k)=x(k-1);
    x(k)=(a(k)*f(b(k))-b(k)*f(a(k)))/(f(b(k))-f(a(k)));
elseif f(a(k-1))*f(x(k-1)) > 0
    a(k)=x(k-1);
    b(k)=b(k-1);
    x(k)=(a(k)*f(b(k))-b(k)*f(a(k)))/(f(b(k))-f(a(k)));
end
fprintf('a(3) = %f\n',a(3));
fprintf('b(3) = %f\n',b(3));
fprintf('x(3) = %f\n\n',x(3));