fprintf('\n\nSistemul 1\n\n');
% Sistemul 1
A = [0 1 1; 2 1 5; 4 2 1];
b = [3; 5; 1];

sol_1_fara = GaussFaraPiv(A,b)
sol_1_part = GaussPivPart(A,b)
sol_1_tot  = GaussPivTot(A,b)

fprintf('\n\nSistemul 2\n\n\n\n');
% Sistemul 2
C = [0 1 -2; 1 -1 1; 1 0 -1];
d = [4; 6; 2];

sol_2_fara = GaussFaraPiv(C,d)
sol_2_part = GaussPivPart(C,d)
sol_2_tot  = GaussPivTot(C,d)