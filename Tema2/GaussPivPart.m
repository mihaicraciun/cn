function [x]=GaussPivPart(A,b)
A=[A,b];% Matricea extinsa
n=size(A,1);
xindice=1:n;%Numerotare initiala necunoscute
for k=1:n-1
    %Calcul maxim
    p = k;
    for i = (k+1):n
        if abs(A(i,k)) > abs(A(p,k))
            p = i;
        end
    end
    if A(p,n) == 0 
        display('Sist. incomp. sau comp. nedet.');
        break
    end
    
    if p~=k
       A([p,k],:) = A([k,p],:);
    end
    for l=k+1:n
        A(l,:) = A(l,:)-A(l,k)/A(k,k)*A(k,:);
    end
end
if A(n,n) == 0
    display('Sist. incomp. sau comp. nedet.')
end
xschimbat=SubsDesc(A(1:n,1:n),A(:,n+1)); 
for i = 1:n
    x(xindice(i)) = xschimbat(i); %Revenire la indicii initiali
end