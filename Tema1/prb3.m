f = inline('exp(x)-2','x')
g = inline('cos(exp(x)-2)','x')
fg = inline('exp(x)-2-cos(exp(x)-2)','x')
A=0.5; B=1.5; eps = 10^(-5);
xaprox=MetBisectie(fg,A,B,eps)
x=linspace(A,B,100);

grid on
hold on

yf=f(x);
plot(x,yf,'k','Linewidth',3) %Constructia graficului  functiei f
xg=linspace(A,B,100);
yg=g(x);
plot(xg,yg,'r','Linewidth',3) %Constructia graficului  functiei g
xfg=linspace(A,B,100);
yfg=fg(x);
plot(xfg,yfg,'c','Linewidth',3) %Constructia graficului  functiei f-g
plot([A B], [0 0],'k','Linewidth',2.4) %Reprezentarea axei Ox
plot([A A], [min(yfg) max(yfg)],'k','Linewidth',2.4); %Reprezentarea axei Oy
plot(xaprox,fg(xaprox),'o','MarkerFaceColor','g','MarkerSize',10)