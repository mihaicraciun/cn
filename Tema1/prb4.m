f = inline('x - sqrt(3)','x')
A=1; B=2; eps = 10^(-5);
xaprox=MetBisectie(f,A,B,eps)
x=linspace(A,B,100);
y=f(3);
%plot(x,y,'Linewidth',3) %Constructia graficului  functiei

grid on
hold on
plot([A B], [0 0],'k','Linewidth',2.4) %Reprezentarea axei Ox
plot([A A], [min(y) max(y)],'k','Linewidth',2.4); %Reprezentarea axei Oy
plot(xaprox,f(xaprox),'o','MarkerFaceColor','r','MarkerSize',10) %Reprezentarea solutiei pe grafic
xlabel('x')
ylabel('y')
title('f(x)=sqrt(3)')