f = inline('x.^3 - 7*x.^2 + 14*x - 6','x');
df = inline('3*x.^2 - 14*x + 14','x');
A=0;
B=4;
eps = 10^(-3);
x01=0.5;
x02=2.5;
x03=3.7;
xaprox1=MetNR(f,df,x01,eps)
xaprox2=MetNR(f,df,x02,eps)
xaprox3=MetNR(f,df,x03,eps)
x=linspace(A,B,100);
y=f(x);
plot(x,y,'Linewidth',3) %Constructia graficului  functiei

grid on
hold on
plot([A B], [0 0],'k','Linewidth',2.4) %Reprezentarea axei Ox
plot([0 0], [min(y) max(y)],'k','Linewidth',2.4); %Reprezentarea axei Oy
plot(xaprox1,f(xaprox1),'o','MarkerFaceColor','g','MarkerSize',10) %Reprezentarea solutiei pe grafic
plot(xaprox2,f(xaprox2),'o','MarkerFaceColor','g','MarkerSize',10) %Reprezentarea solutiei pe grafic
plot(xaprox3,f(xaprox3),'o','MarkerFaceColor','g','MarkerSize',10) %Reprezentarea solutiei pe grafic
xlabel('x')
ylabel('y')
title('f(x)=x.^3 - 7*x.^2 + 14*x - 6')
