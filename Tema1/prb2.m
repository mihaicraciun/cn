f = inline('x.^3 - 7*x.^2 + 14*x - 6','x')
A=0; B=4; eps = 10^(-5);
xaprox=MetBisectie(f,A,B,eps)
x=linspace(A,B,100);
y=f(x);
plot(x,y,'Linewidth',3) %Constructia graficului  functiei

grid on
hold on
plot([A B], [0 0],'k','Linewidth',2.4) %Reprezentarea axei Ox
plot([0 0], [min(y) max(y)],'k','Linewidth',2.4); %Reprezentarea axei Oy
%plot(xaprox,f(xaprox),'o','MarkerFaceColor','g','MarkerSize',10) %Reprezentarea solutiei pe grafic
r1 = MetBisectie(f,0,1,eps)
r2 = MetBisectie(f,1,3.2,eps)
r3 = MetBisectie(f,3.2,4,eps)
plot(r1,f(r1),'*')
plot(r2,f(r2),'*')
plot(r3,f(r3),'*')
xlabel('x')
ylabel('y')
title('f(x)=x^3 - 7*x^2 + 14*x - 6')