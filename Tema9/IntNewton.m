function I = IntNewton( f, a, b )
%INTNEWTON Summary of this function goes here
%   Detailed explanation goes here
I = (f(a) + 3*f((2*a+b)/3) + 3*f((a+2*b)/3) + f(b)) * (b - a) / 8;
end
