function I = IntSimpson( f, a, b )
%INTSIMPSON Summary of this function goes here
%   Detailed explanation goes here
I = (f(a) + 4*f((a+b)/2) + f(b)) * (b - a)/6;
end
