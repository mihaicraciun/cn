function [ df ] = MetRichardson( f, x, h, n, phi )
%METRICHARDSON Summary of this function goes here
%   Detailed explanation goes here
q = zeros(n);
for i = 1 : n
    q(i, 1) = phi(f, x, h/(2^(i-1)));
end
for i = 2 : n
    for j = 2 : i
        q(i, j) = q(i, j-1) + (q(i, j-1) - q(i-1, j-1))/(2^(j-1) - 1);
    end
end
df = q(n, n);
end

