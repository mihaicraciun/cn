xfunction I = Integrare( f, a, b, m, metoda )
%INTEGRARE Summary of this function goes here
%   Detailed explanation goes here
I = 0;
x = @(a, k, h) (a + (k-1) * h);
switch metoda
    case 'dreptunghi'
        h = (b - a) / (2*m);
        for k = 1 : m
            I = I + IntDreptunghi(f, x(a, 2*k-1, h), x(a, 2*k+1, h));
        end
    case 'trapez'
        h = (b - a) / m;
        for k = 1 : m
            I = I + IntTrapez(f, x(a, k, h), x(a, k+1, h));
        end
    case 'Simpson'
        h = (b - a) / (2*m);
        for k = 1 : m
            I = I + IntSimpson(f, x(a, 2*k-1, h), x(a, 2*k+1, h));
        end
    case 'Newton'
        h = (b - a) / (3*m);
        for k = 1 : m
            I = I + IntNewton(f, x(a, 3*k-2, h), x(a, 3*k+1, h));
        end
    otherwise
        I = 'necunoscut';
end
end

