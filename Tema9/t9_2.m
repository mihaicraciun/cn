phi1 = @(f, x, h) ( (f(x+h) - f(x))/h );
phi2 = @(f, x, h) ( (f(x+h) - 2*f(x) + f(x-h))/(h^2) );
a = 0; b = pi;
f = @(x) (sin(x));
df= @(x) (cos(x));
d2f=@(x) ( -f(x));
n = [4 6 8];
m = 100;
i = 1;
h = 0.1;
x = linspace(a, b, m);
y = zeros(1, m);
for j = 1 : m
    y(j) = MetRichardson(f, x(j), h, n(i), phi1);
end
hold on;
dy = df(x);
plot(x, dy);
plot(x, y, 'o', 'markersize', 10);
legend('df', 'richardson');
% c)
figure;
plot(x, abs(y-dy));
legend('diferenta in modul a erorii');
% d)
y = zeros(1, m);
for j = 1 : m
    y(j) = MetRichardson(f, x(j), h, n(i) - 1, phi2);
end
figure;
hold on;
d2y = d2f(x);
plot(x, d2y);
plot(x, y, 'o', 'markersize', 10);
legend('d2f', 'richardson');



