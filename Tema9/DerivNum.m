function dy = DerivNum( x, y, metoda )
%DERIVNUME Summary of this function goes here
%   Detailed explanation goes here
m = numel(y) - 1;
dy = zeros(1, m-1);
switch metoda
    case 'diferente finite progresive'
        for i = 2 : m
            dy(i-1) = (y(i+1) - y(i)) / (x(i+1) - x(i));
        end
    case 'diferente finite regresive'
        for i = 2 : m
            dy(i-1) = (y(i) - y(i-1)) / (x(i) - x(i-1));
        end
    case 'diferente finite centrale'
        for i = 2 : m
            dy(i-1) = (y(i+1) - y(i-1)) / (x(i+1) - x(i-1));
        end
    otherwise
        dy = 'nu stim ce a vrut sa spuna poetul';
end
end

